# Install rustup

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
## Install ubuntu dependancies

```bash
apt-get install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3
```

## Clone alacritty

```bash
git clone https://github.com/alacritty/alacritty.git
cd alacritty
```
## Verify any changes in INSTALL.md before performing all proceeding tasks below within git repo

```bash
ls
alacritty                alacritty.yml  CHANGELOG.md     extra           Makefile      scripts
alacritty_config_derive  Cargo.lock     CONTRIBUTING.md  INSTALL.md      README.md
alacritty_terminal       Cargo.toml     docs             LICENSE-APACHE  rustfmt.toml
```

## Universal Binary

The following will build an executable that runs on both x86 and ARM macos
architectures:

```sh
rustup target add x86_64-apple-darwin aarch64-apple-darwin
make app-universal
```

## Linux / Windows

```sh
cargo build --release
```

## Set PATH to ALACRITTY and verify rustup cargo

```bash
. "$HOME/.cargo/env"

export ALACRITTY=$HOME/.alacritty
export PATH=$PATH:$ALACRITTY/target/release
```



