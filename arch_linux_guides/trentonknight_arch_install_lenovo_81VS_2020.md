# ARCH LINUX INSTALL 2018
> Set bios to uefi boot only
## Temporary network connection for build
``` bash
iwctl
device list
station wlan0 scan
station wlan0 get-networks
station wlan0 connect SSID
ip link
ping www.linux.org
```


## Verify UEFI drivers loaded
``` bash
ls /sys/firmware/efi/efivars
```

## Setup local time 
``` bash
timedatectl set-ntp true
timedatectl set-timezone America/New_York
timedatectl status
```

## Clean up hard disk for build
``` bash
gdisk /dev/mmcblk0
gdisk /dev/sdb #Or sda, sdc verify what microSD mounts as.
```
* x, z, Y

## Create partitions with external SD Card

``` bash
gdisk /dev/mmblk0
```
* /dev/mmcblk0p1 
  * +500M EFI System (Or everything if you wish)
  * t EF00

``` bash
gdisk /dev/adb
```

* /dev/sdb `
  * Remaining
  * t 8300

## Format Partitions
``` bash 
mkfs.vfat -F32 /dev/mmcblk0p1
mkfs.ext4 /dev/sdb
```

## Mount partitions to liveusb /mnt and /boot folders
### Both mount will be at the root / of each drive
``` bash
mount /dev/mmcblk0p1 /boot
mount /dev/sdb /mnt
```
## Find and use best mirrors for packages before installing base system
```bash
pacman -Sy reflector
reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
```

``` bash 
pacstrap /mnt base linux linux-firmware linux-headers dhcpcd vim networkmanager grub-efi-x86_64 efibootmgr sudo
```
``` bash
genfstab -U /mnt > /mnt/etc/fstab
genfstab -U /boot > /mnt/etc/fstab
```
Append microSD manually to /etc/fstab now or later after build. Use ext4 partition for microSD card.
``` bash
# <device>    <dir>   <type> <options> <dump> <fsck>
/dev/mmcblk0p1 /   vfat   defaults    0     2
/dev/mmcblk0p2      /       ext4   defaults    0     1
/dev/sda               /run/media/microSD ext4 rw,relatime 03
```

## Verify all partitions are similar to the example shown above
``` bash
cat /mnt/etc/fstab
blkid
```
## Enter /mnt for edits which will affect build after first reboot
``` bash
arch-chroot /mnt /bin/bash
```
## Setup timezone
``` bash
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc --utc
echo archboX-lenovo > /etc/hostname
vim /etc/hosts
```
``` bash
127.0.0.1    localhost
::1        localhost
127.0.1.1    archboX-lenovo.localdomain    archboX-lenovo
```
## Uncomment en_US.UTF-8 UTF-8 and other needed localizations in 
``` bash
/etc/locale.gen
```
## Generate locale with: 
``` bash
locale-gen
```
## Setup password for root
``` bash
passwd
```
> Exit chroot
``` bash
exit
```
Grub only recongnizes what you have mounted not the physical drive. Fstab for both /mnt and /boot are at the root / of the two hard drives used while grub-install uses the mounted directories.
``` bash
# <device>    <dir>   <type> <options> <dump> <fsck>
/dev/mmcblk0p1 /   vfat   defaults    0     2
/dev/mmcblk0p2      /       ext4   defaults    0     1
```
## Grub install outside of chroot
``` bash
grub-install --target=x86_64-efi --root-directory=/mnt --efi-directory=/boot --bootloader-id=GRUB
```
## Grub mkconfig within arch-chroot
``` bash
arch-chroot /mnt /bin/bash
```
``` bash
grub-mkconfig -o /boot/grub/grub.cfg
```
## Exit chroot again
``` bash
exit
```
## Verify drives, umount and reboot
``` bash
lsblk -l #review mounted drives
umount -R /dev/mmcblk0p1 /boot
umount -R /dev/mmcblk0p2 /mnt
reboot
```
## Basic Post Install setup
### Add standard user
``` bash
useradd -m -g users -G wheel -s /bin/bash trentonknight
passwd trentonknight
```
### Add standard user to sudoers
```bash
vim /etc/sudoers
trentonknight ALL=(ALL) ALL
```
# Connect to network
```bash
sudo nmcli dev wifi
sudo nmcli dev wifi --ask connect SSIDnamehere 
```

