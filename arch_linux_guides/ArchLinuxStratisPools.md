# STRATIS local storage management on Arch Linux
* [STRATIS project homepage](https://stratis-storage.github.io/)
# Install stratis tools
```bash
pacman -Sy stratisd stratis-cli
```
# Check version
```bash
stratis --version
```
#Enable stratis
```bash
systemctl --now enable stratisd
systemctl status stratisd --no-pager
stratis daemon version
```
## Original tutorial used for this effort may be found here: [https://stratis-storage.github.io/howto/](https://stratis-storage.github.io/howto/)
#Check block devices
```bash
lsblk
```
# Begin clearing first block device a 1TB TOSHIBA DT01ACA100 standard hard drive.
```bash
sudo blkid -p /dev/sda
sudo wipefs -a /dev/sda
```

# Begin clearing second and separate block device. A Western Digital PC SN530 NVMe SSD. 
```bash
sudo blkid -p /dev/nvme0n1
sudo wipefs -a /dev/nvme0n1
```
# For my needs I will be using both aforementioned block devices to create a pool with two devices together.

```bash
sudo stratis pool create pool_sda_nvme0n1 /dev/sda /dev/nvme0n1
```
#Destroy pool
```bash
sudo stratis pool destroy pool_sda_nvme0n1
```
# Create pool with single drive
```bash
sudo stratis pool create poolAlpha /dev/sda
sudo stratis pool list
```
# Create a second pool with second block device then destroy after viewing
```bash
sudo stratis pool create poolBravo /dev/nvme0n1
sudo stratis pool list
sudo stratis pool destroy poolBravo
```
# Now use second block device as cache
```bash
sudo stratis pool destroy poolBravo
stratis blockdev list
```
# Create a filesystem
```bash
sudo stratis filesystem create poolAlpha fileSysAlpha
sudo stratis fs
```
# Deleting filesystems
```bash
sudo stratis filesystem destroy poolAlpha fileSysAlpha
```
# Mount filesystem and verify read and write
```bash
sudo mkdir /mnt/pool
sudo mount /dev/stratis/poolAlpha/fileSysAlpha /mnt/pool
sudo touch /mnt/pool/hello_text.txt
ls /mnt/pool/
sudo umount /dev/stratis/poolAlpha/fileSysAlpha /mnt/pool
ls /mnt/pool/
sudo mount /dev/stratis/poolAlpha/fileSysAlpha /mnt/pool
ls /mnt/pool/
```
#Run mount command to view mount
```bash
/dev/mapper/stratis-1-68e39ee0bc3c46a1946acb618790522d-thin-fs-a010e0fcee78428191bc76eeda786889 on /mnt/pool type xfs (rw,relatime,attr2,inode64,logbufs=8,logbsize=32k,sunit=2048,swidth=2048,noquota)
```
#Run lsblk to verify and grab UUID off physical block device
```bash
lsblk --output=UUID /dev/sda
```
#Edit for Fstab or future use
```bash
vim /etc/fstab
```
Append to fstab similar result from mount command and lsblk UUID discovered. Another words your physical block device UUID, your chosen mount point, the file type is xfs, dafault settings systemd is required for the pool to exist. Everything below should be the same on Arch Linux for you with a different UUID and possibly a different custom mount point to created. 
```bash
UUID=a010e0fc-ee78-4281-91bc-76eeda786889 /mnt/pool xfs defaults,x-systemd.requires=stratisd.service 0 0
```
# View mounted pool after reboot with new fstab edits
```bash
df -h 
```
You should see something similar to the below
```bash
/dev/mapper/stratis-1-68e39ee0bc3c46a1946acb618790522d-thin-fs-a010e0fcee78428191bc76eeda786889  1.0T  7.2G 1017G   1% /mnt/pool
```
Check stratis as well. 
```bash
sudo stratis blockdev list
sudo stratis pool list
sudo stratis filesystem list
```




