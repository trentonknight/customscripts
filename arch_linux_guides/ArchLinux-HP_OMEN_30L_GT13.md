# ARCH LINUX INSTALL OMEN 30L Desktop GT13
* F2 is Diagnostics
* F9 is select boot devices
* F10 is UEFI menu to make changes similar to old BIOS menus (Hit CANCEL to access this menu)
## Temporary network connection for build
``` bash
iwctl
device list
station wlan0 scan
station wlan0 get-networks
station wlan0 connect SSID
ip link
ping www.linux.org
```


## Verify UEFI drivers loaded
``` bash
ls /sys/firmware/efi/efivars
```

## Setup local time 
``` bash
timedatectl set-ntp true
timedatectl set-timezone America/New_York
timedatectl status
```
## 
```bash
/dev/nvme0n1p1 = EFI 256MB
/dev/nvme0n1p2 = ext4 512MB
/dev/nvme0n1p3 = ext4 Remaining Drive
```

## Clean up hard disk and remove old UEFI for build
``` bash
efibootmgr -b 0 -B
parted -l /dev/nvme0n1
```

## Create partitions
``` bash
fdisk /dev/sdb
```
* /dev/nvme0n1p1
  * +256M
  * t EF00
* /dev/nvme0n1p2
  * +512M
  * t 8300
* /dev/sdb3
  * Remaining Block Device Space
  * t 8300

## Format and encrypt partitions
### Encrypt nvme0n1p3 first 
``` bash
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 /dev/nvme0n1p3
cryptsetup open /dev/nvme0n1p3 luks_root
```
### Format partitions correctly including the luks mapped drive
```bash
mkfs.vfat -n "EFI" -F32 /dev/nvme0n1p1
mkfs.ext4 -L BOOT /dev/nvme0n1p2
mkfs.ext4 -L ROOT /dev/mapper/luks_root
```
## Mount from liveusb, create directories and begin image build
```bash
mount /dev/mapper/luks_root /mnt
mkdir -p /mnt/boot/efi
mount /dev/nvme0n1p2 /mnt/boot
mount /dev/nvme0n1p1 /mnt/boot/efi
```
### Add swap space
```bash
dd if=/dev/zero of=swap bs=1M count=1024
mkswap swap
chmod 0600 swap
swapon swap
```
## Find and use best mirrors for packages before installing base system
```bash
pacman -Sy reflector
reflector --country 'United States' --latest 50 --protocol https --download-timeout 60 --save /etc/pacman.d/mirrorlist
```

``` bash 
pacstrap /mnt base base-devel linux-lts linux linux-firmware linux-headers dhcpcd vim networkmanager grub efibootmgr sudo xf86-video-nouveau ntp
```
# Add all mount point begining at /mnt 
``` bash
genfstab -U /mnt > /mnt/etc/fstab 
```
# /etc/fstab should look something like this on a single hard drive 
# <device>    <dir>   <type> <options> <dump> <fsck>
``` bash
# /dev/mapper/luks_root LABEL=root
UUID=c4f2da99-a6bc-4757-a465-ad2ff83415c0       /               ext4            rw,relatime     0 1

# /dev/nvme0n1p2 LABEL=boot
UUID=fb031908-d377-4248-a14c-f7abb32c82e4       /boot           ext4            rw,relatime     0 2

# /dev/nvme0n1p1 LABEL=EFI
UUID=7C44-604D          /boot/efi       vfat            rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro   0 2

/swap                   none            swap            defaults        0 0
```

## Verify all partitions are similar to the example shown above
``` bash
cat /mnt/etc/fstab
blkid
```
## Enter /mnt for edits which will affect build after first reboot
``` bash
arch-chroot /mnt /bin/bash
passwd
```
## Setup timezone
``` bash
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc --utc
echo archboX > /etc/hostname
vim /etc/hosts
```
``` bash
127.0.0.1    localhost
::1        localhost
127.0.1.1    archboX.localdomain    archboX
```
## Uncomment en_US.UTF-8 UTF-8 and other needed localizations in 
``` bash
/etc/locale.gen
```
## Generate locale with: 
``` bash
locale-gen
```
## GRUB configuration for encryption
```bash
vim /etc/default/grub
```
### Append cryptdevice 
```bash
GRUB_CMDLINE_LINUX="cryptdevice=/dev/nvme0n1p3:luks_root"
# Uncomment to disable graphical terminal
GRUB_TERMINAL_OUTPUT=console
```
Remove quiet from GRUB_CMDLINE_LINUX_DEFAULT variable
```bash
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"
```
to
```bash
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 "
```

## Edit mkinitcpio.conf to allow encrypt
```bash
MODULES=(nouveau)
HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)
```
### Run mkinitcpio
```bash
mkinitcpio -p linux
```
## GRUB install on to /boot aka /dev/nvme0n1p2 in this example
```bash
 grub-install --boot-directory=/boot --efi-directory=/boot/efi
 grub-mkconfig -o /boot/grub/grub.cfg
 grub-mkconfig -o /boot/efi/EFI/arch/grub.cfg
```
## Exit, reboot and hold your breath
```bash
exit
reboot
```
## Verify no nvidia modules are being used, unless you want them
```bash
lsmod | grep nvidia
lsmod | grep nv
```
### Example blacklist of nvidia modules, may NOT be needed
```bash
vim /etc/modprobe.d/blacklist_i2c-nvidia-gpu.conf
blacklist i2c_nvidia_gpu
```
#### GDM blinking cursor fix for nvidia cards
```bash
sudo vim /usr/lib/systemd/system/gdm.service
```
##### Append to gdm.service
```bash
[Service]
Type=idle
ExecStartPre=/bin/sleep 1
```
##### Edit or create 20-nvidia.conf
```bash
vim /etc/X11/xorg.conf.d/20-nvidia.conf
```
##### Append to 20-nvidia.conf
```bash
Section "Device"
Identifier "Nvidia Card"
Driver "nvidia"
VendorName "NVIDIA Corporation"
BoardName "NVIDIA GeForce RTX 2060 Rev. A"
EndSection
```
#REFERENCES
[https://linuxhint.com/setup-luks-encryption-on-arch-linux/](https://linuxhint.com/setup-luks-encryption-on-arch-linux/)
