#Installing opensc
```bash
pacman -S ccid opensc
sudo systemctl enable pcscd
sudo systemctl start pcscd
```
# Add the following to /etc/opensc.conf to force the selection cac bas
```bash
card_drivers = cac, internal
Force_card_driver = cac;
```
#Load Opensc firefox Device Manager in Security Modules and Devices
```bash
/usr/lib/pkcs11/opensc-pkcs11.so
```
#vmware-view install
## Install keymaps first
```bash
git clone https://aur.archlinux.org/vmware-keymaps.git
```
## Now update sha256sum and install vmware horizon client and remaining dependancies
```bash
git clone https://aur.archlinux.org/vmware-horizon-client.git
makpkg -g
```
Copy resultant checksum and update the PKGBUILD file then run makepkg -si
```bash
makepkg -si
```
#Static link Opensc
```bash
mkdir /usr/lib/vmware/view/pkcs11/
sudo ln -s /usr/lib/pkcs11/opensc-pkcs11.so /usr/lib/vmware/view/pkcs11/libgtop11dotnet.so
```
# Finally run vmware-view
```bash
sudo vmware-view -s https://VDI-East.uscg.mil --rdesktopOptions="-r scard" --usbAutoConnectAtStartUp=TRUE
```
# Dockerized version
## Allow GUI openning with sudo
```bash
xhost +
```
If that does not work try
```bash
export DISPLAY='IP:0.0'
```
# Run vmware-horizon-docker with opensc-pkcs11.so
```bash
podman run -it \
            --privileged \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v ${HOME}/.vmware:/root/.vmware/ \
            -v /usr/lib/vmware/view/pkcs11/libgtop11dotnet.so:/usr/lib/pkcs11/opensc-pkcs11.so \
            -v /etc/localtime:/etc/localtime:ro \
            -v /dev/bus/usb:/dev/bus/usb \
            -e DISPLAY=$DISPLAY \
            --device /dev/snd \
            exotime/vmware-horizon-docker

