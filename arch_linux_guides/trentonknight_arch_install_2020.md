# ARCH LINUX INSTALL 2020
> Set bios to uefi boot only
## Temporary network connection for build
``` bash
wifi-menu
ip link
ping www.linux.org
```
## Verify UEFI drivers loaded
``` bash
ls /sys/firmware/efi/efivars
```

## Setup local time 
``` bash
timedatectl set-ntp true
timedatectl set-timezone America/New_York
timedatectl status
```

## Clean up hard disks for build
``` bash
gdisk /dev/sdc
```
* x, z, Y,

## Create partitions
``` bash
gdisk /dev/sdc
```
* /dev/sdc1 
  * +500M EFI System
  * t EF00
* /dev/sdc2
  * +1G Linux Swap
  * t 8200
* /dev/sdc3
  * Remaining space on sdb
  * default
* w
## Format Partitions
``` bash 
mkfs.vfat -F32 /dev/sdc1
mkswap /dev/sdc2
swapon /dev/sdc2
mkfs.ext4 /dev/sdc3
```

## Mount partitions to liveusb /mnt and /boot folders
### Both mount will be at the root / of each drive
``` bash
mount /dev/sdc3 /mnt
mount /dev/sdc1 /boot
```
## Find and use best mirrors for packages before installing base system
```bash
pacman -S reflector
```
``` bash
reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
```

``` bash 
pacstrap /mnt base linux linux-firmware linux-headers dhcpcd vim wpa_supplicant grub-efi-x86_64 efibootmgr 
```
``` bash
genfstab -L /mnt > /mnt/etc/fstab
genfstab -L /boot > /mnt/etc/fstab
```
``` bash
# <device>    <dir>   <type> <options> <dump> <fsck>
/dev/sdc1       /       vfat   defaults    0     2
/dev/sdc3       /       ext4   defaults    0     1
/dev/sdc2     none      swap   defaults    0     0
```

## Verify all partitions are similar to the example shown above
``` bash
cat /mnt/etc/fstab
blkid
```
## Enter /mnt for edits which will affect build after first reboot
``` bash
arch-chroot /mnt /bin/bash
```
## Setup timezone
``` bash
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc --utc
echo archboX2 > /etc/hostname
vim /etc/hosts
```
``` bash
127.0.0.1    localhost
::1          localhost
127.0.1.1    archboX2.localdomain    archboX2
```
## Uncomment en_US.UTF-8 UTF-8 and other needed localizations in 
``` bash
/etc/locale.gen
```
## Generate locale with: 
``` bash
locale-gen
```
## Setup password for root
``` bash
passwd
```
> Exit chroot
``` bash
exit
```
Grub only recongnizes what you have mounted not the physical drive. Fstab for both /mnt and /boot are at the root / of the two hard drives used while grub-install uses the mounted directories.
``` bash
# <device>    <dir>   <type> <options> <dump> <fsck>
/dev/sdc1       /       vfat   defaults    0     2
/dev/sdc3       /       ext4   defaults    0     1
/dev/sdc2     none      swap   defaults    0     0
```
## Grub install outside of chroot
``` bash
grub-install --target=x86_64-efi --root-directory=/mnt --efi-directory=/boot --bootloader-id=GRUB
```
## Grub mkconfig within arch-chroot
``` bash
arch-chroot /mnt /bin/bash
```
``` bash
grub-mkconfig -o /boot/grub/grub.cfg
```
## Exit chroot again
``` bash
exit
```
## Verify drives, umount and reboot
``` bash
lsblk -l #review mounted drives
umount -R /dev/sdc1 /boot
umount -R /dev/sdc3 /mnt
reboot
```
