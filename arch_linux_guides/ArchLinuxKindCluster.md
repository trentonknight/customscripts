# Install golang, kind, docker and emoji fonts
```bash
pacman -Sy go docker noto-fonts-emoji gnu-free-fonts ttf-arphic-uming ttf-indic-otf ttf-joypixels
GO111MODULE="on" go get sigs.k8s.io/kind@v0.8.1
sudo systemctl enable docker
sudo systemctl start docker
sudo systemctl status docker
```
## Add trusted admin to docker group
```bash
sudo gpasswd -a username docker
```
## Install Krew
Assuming git is installed run the following in terminal.
```bash
(
  set -x; cd "$(mktemp -d)" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.tar.gz" &&
  tar zxvf krew.tar.gz &&
  KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
  "$KREW" install krew
)
```
## Edit .bashrc for GOPATH for Kind install and Krew
```bash
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
export PATH="${PATH}:${HOME}/.krew/bin
```
# Kind Create Cluster
```bash
kind create cluster # Default cluster context name is `kind`.
```
## Cluster with custom name
```
kind create cluster --name kind
```
## View Clusters
```bash
kind get clusters
```
# Deploy KubeVirt
```bash
export KUBEVIRT_VERSION=$(curl -s https://api.github.com/repos/kubevirt/kubevirt/releases | grep tag_name | grep -v -- - | sort -V | tail -1 | awk -F':' '{print $2}' | sed 's/,//' | xargs)
```
### Deploy KubeVirt Operator
```bash
kubectl create -f https://github.com/kubevirt/kubevirt/releases/download/${KUBEVIRT_VERSION}/kubevirt-operator.yaml
```
### Verify its running
```bash
kubectl get pods -n kubevirt
```
#### Deploy KubeVirt
```bash
kubectl create -f https://github.com/kubevirt/kubevirt/releases/download/${KUBEVIRT_VERSION}/kubevirt-cr.yaml
```
##### Verify deployment
```bash
kubectl get pods -n kubevirt
```
# Download virtctl tool (Used for serial console)
```bash
curl -L -o virtctl \
    https://github.com/kubevirt/kubevirt/releases/download/${KUBEVIRT_VERSION}/virtctl-${KUBEVIRT_VERSION}-linux-amd64
```
## mv and chmod virtctl
```bash
sudo mv virctl /usr/bin
sudo chmod 755 /usr/bin/virtctl
```
# Update Krew NOT as sudo
```bash
kubectl krew update
kubectl krew install virt
```
# Build a Virtual Machine
```bash
wget https://raw.githubusercontent.com/kubevirt/kubevirt.github.io/master/labs/manifests/vm.yaml
less vm.yaml
```
Apply Manifest
```bash
kubectl apply -f https://raw.githubusercontent.com/kubevirt/kubevirt.github.io/master/labs/manifests/vm.yaml
```
## List Virtual Machines
```bash
kubectl get vms
```
Or full details
```bash
kubectl get vms -o yaml testvm
```
## Start chosen Machine and use serial console to login
```bash
kubectl virt start testvm
kubectl get vmis
sudo virtctl console testvm
```
# Install Cockpit and packagekit for updates.
```bash
pacman -Sy cockpit packagekit
systemctl enable --now cockpit.socket
systemctl start cockpit.socket
systemctl status cockpit.socket
firefox https://localhost:9090/
```
Use system username and pasword to login



# REFERENCES
* [https://kubevirt.io/quickstart_kind/](https://kubevirt.io/quickstart_kind/)
* [https://kind.sigs.k8s.io/docs/user/quick-start/](https://kind.sigs.k8s.io/docs/user/quick-start/)
