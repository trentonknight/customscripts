# ARCH LINUX INSTALL 2018
> Set bios to uefi boot only
## Temporary network connection for build
``` bash
wifi-menu
ip link
ping www.linux.org
```


## Verify UEFI drivers loaded
``` bash
ls /sys/firmware/efi/efivars
```

## Setup local time 
``` bash
timedatectl set-ntp true
timedatectl set-timezone America/New_York
timedatectl status
```

## Clean up hard disks for build
``` bash
gdisk /dev/sdb5
```
* x, z, Y,
``` bash
gdisk /dev/sdb6
```
* x, z, Y
## Create partitions
``` bash
gdisk /dev/sdb5
```
* /dev/sdb5 
  * +500M EFI System
  * t 1
* /dev/sdb6
  * t 8200

## Format Partitions
``` bash 
mkfs.vfat -F32 /dev/sdb5
mkfs.ext4 /dev/sdb6
```

## Mount partitions to liveusb /mnt and /boot folders
### Both mount will be at the root / of each drive
``` bash
mount /dev/sdb6 /mnt
mount /dev/sdb5 /boot
```
## Find and use best mirrors for packages before installing base system
```bash
pacman -Sy reflector 
relector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
```

``` bash 
pacman -Sy git

git clone https://aur.archlinux.org/rtl8821ce-dkms-git.git

pacstrap /mnt base linux linux-firmware linux-headers dhcpcd vim networkmanager grub-efi-x86_64 efibootmgr sudo dkms linux-headers

cd rtl8821ce-dkms-git/

makepkg -si
```
``` bash
genfstab -L /mnt > /mnt/etc/fstab
genfstab -L /boot > /mnt/etc/fstab
```
``` bash
# <device>    <dir>   <type> <options> <dump> <fsck>
/dev/sdb5 /   vfat   defaults    0     2
/dev/sdb6       /       ext4   defaults    0     1
```

## Verify all partitions are similar to the example shown above
``` bash
cat /mnt/etc/fstab
blkid
```
## Enter /mnt for edits which will affect build after first reboot
``` bash
arch-chroot /mnt /bin/bash
```
## Setup timezone
``` bash
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc --utc
echo archboX > /etc/hostname
vim /etc/hosts
```
``` bash
127.0.0.1    localhost
::1        localhost
127.0.1.1    archboX.localdomain    archboX
```
## Uncomment en_US.UTF-8 UTF-8 and other needed localizations in 
``` bash
/etc/locale.gen
```
## Generate locale with: 
``` bash
locale-gen
```
## Setup password for root
``` bash
passwd
```
> Exit chroot
``` bash
exit
```
Grub only recongnizes what you have mounted not the physical drive. Fstab for both /mnt and /boot are at the root / of the two hard drives used while grub-install uses the mounted directories.
## Grub install outside of chroot
``` bash
grub-install --target=x86_64-efi --root-directory=/mnt --efi-directory=/boot --bootloader-id=GRUB
```
## Grub mkconfig within arch-chroot
``` bash
arch-chroot /mnt /bin/bash
```
``` bash
grub-mkconfig -o /boot/grub/grub.cfg
```
## Exit chroot again
``` bash
exit
```
## Verify drives, umount and reboot
``` bash
lsblk -l #review mounted drives
umount -R /dev/sdb5 /boot
umount -R /dev/sdb6 /mnt
reboot
```
## Basic Post Install setup
### Add standard user
``` bash
useradd -m -g users -G wheel -s /bin/bash trentonknight
passwd trentonknight
```
### Add standard user to sudoers
```bash
vim /etc/sudoers
trentonknight ALL=(ALL) ALL
```
# Connect to network
```bash
sudo nmcli dev wifi
sudo nmcli dev wifi connect SSIDnamehere password MyPasswordHere
```

