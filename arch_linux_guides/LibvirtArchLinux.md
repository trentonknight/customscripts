# Review latest documentation on wiki archlinux first
* [https://wiki.archlinux.org/index.php/Libvirt](https://wiki.archlinux.org/index.php/Libvirt)
# Install network connectivty packages and libvirt
```bash
pacman -Sy libvirt ebtables dnsmasq bridge-utils openbsd-netcat dmidecode
```
## Start libvirtd service
```bash
systemctl enable libvirtd
systemctl start libvirtd
systemctl status libvirtd
```

# Install virt-manager
```bash
pacman -Sy virt-manager
```

# Add yourself to Libvirt group
```bash
sudo gpasswd -a trentonknight libvirt
cat /etc/group
```

# Connecting using URI
```bash
sudo virsh -c 'qemu+ssh://virtuser@host2/system'
```
# Networking
```bash
sudo virsh net-list --all
sudo virsh net-autostart default
sudo virsh net-start default
```
## Viewing current dhcp leases
Assuming network name is default
```bash
sudo virsh net-dhcp-leases default
```
# List VMs in virsh shell
```bash
list --all
```
# Start VM in virsh shell
```bash
start VMname
```
# GUI view
```bash
pacman -Sy virt-viewer
virt-viewer --connect 'qemu+ssh://virtuser@host2/system'
```
# Optional Networking Topics
## Setup local DNS
```bash
systemctl enable systemd-resolved 
systemctl start systemd-resolved 
systemctl status systemd-resolved 
```

## TLS and Certs

* [https://libvirt.org/remote.html#Remote_TLS_CA](https://libvirt.org/remote.html#Remote_TLS_CA)
* [https://libvirt.org/tlscerts.html](https://libvirt.org/tlscerts.html)
* [https://www.gnutls.org/manual/html_node/certtool-Invocation.html] (https://www.gnutls.org/manual/html_node/certtool-Invocation.html)
```bash
pacman -Sy gnutls
```
```bash
certtool --generate-privkey > cakey.pem
```
Create a ca.info file
```bash
cn = Name of Organization
ca
cert_signing_key
```
Run certool to generate cacert.pem
```bash
certtool --generate-self-signed --load-privkey cakey.pem \
  --template ca.info --outfile cacert.pem
```
Create CA directory
```bash
sudo mkdir -p /etc/pki/CA
sudo chmod -R 755 /etc/pki/CA
sudo mv cacert.pem /etc/pki/CA/
```
