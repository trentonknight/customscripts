# The following is a custom guide on installing the OpenVPN3 client on Arch Linux. I have created a custom Service which you may edit to match your environment. 
Linux Instructions on OpenVPN website
* [https://community.openvpn.net/openvpn/wiki/OpenVPN3Linux](https://community.openvpn.net/openvpn/wiki/OpenVPN3Linux)
## Download .ovpn profile from My Devices on your OpenVPN3 account online:
* [https://openvpn.cloud.cloud/tenant](https://openvpn.cloud.cloud/tenant)
For example my URL is the following now that my account was created. Replace trentonknight with your account name:
* [https://trentonknight.openvpn.cloud/devices](https://trentonknight.openvpn.cloud/devices)
 If needed use nmcli from NetworkManager to manage or view wireless connections

Install the OpenVPN3 dependancies for Arch Linux

``` bash
pacman -Sy openssl glib2 jsoncpp libcap-ng \
liblzf util-linux tinyxml2 python \
python-docutils python-jinja pkgconfig autoconf \
autoconf-archive automake pkg-config networkmanager \
gcc make cmake
```

clone the git repository on GitHub first
``` bash
git clone https://github.com/OpenVPN/openvpn3-linux.git
```

Create the needed OpenVPN3 Linux Client group
``` bash
./bootstrap.sh 
groupadd -r openvpn     
useradd -r -s /sbin/nologin -g openvpn openvpn
```



```
Compile OpenVPN3 Linux Client source code
``` bash
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var
```
Use the following configuration for AWS-VPC integration
``` bash
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-addons-aws
```
Use the following configuration for Debugging
``` bash
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-debug-options
```
Use the following configuration for all
``` bash
./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var --enable-debug-options --enable-addons-aws
```
Run Make
``` bash
make
make install
```
# Restart DBUS
``` bash
systemctl reload dbus
```
*For informational reasons only you should know that the OpenVPN3 DBUS systems services with various names similar to net.openvpn.v3.service exist here:*
```bash
ls /usr/share/dbus-1/system-services/
```
Openvpn3 with your downloded configuration file from the hosted openvpn cloud provider:
```bash
openvpn3 session-start --config yourhostnamehere_ashburn_va.ovpn
```
View active session
```bash
openvpn3 sessions-list
```
Create Systemd service to auto start OpenVPN3 on reboot. Replace the shown ovpn file to match yours or this will fail.
```bash
mkdir ~/.config/openvpn3
mv yourhostnamehere_ashburn_va.ovpn ~/.config/openvpn3
```
Create ovpn3.sh script for service to run
```bash
#!/bin/bash
openvpn3 session-start --config ~/.config/openvpn3/yourhostnamehere_ashburn_va.ovpn
```
Double check permissions to your new script:
```bash
sudo chmod +x -R ~/.config/openvpn3
ls -l ~/.config/openvpn3/
```
Create Service 
```bash
touch /etc/systemd/system/ovpn3.service
vim /etc/systemd/system/ovpn3.service
```
Append the following into new ovpn3.service. Edit the After variable to match your wireless or ethernet connected device used for connectivity. For example sys-subsystem-net-devices-wlp2s0.device shown in the below service as sys-subsystem-net-devices-yournetworkdevicehere.device. Check your active networking device:
```bash
ip addr
```
Then verify the matching device that is plugged in and being used by Systemd
```bash
systemctl --type=device --state=plugged
```
Edit the below service ensuring the After variable matches your chosen networking device:
```bash
[Unit]
Description=Openvpn3 Auto Start on Boot
After=sys-subsystem-net-devices-yournetworkdevicenamehere.device


[Service]
Type=Idle
RemainAfterExit=yes
ExecStart=$HOME/.config/openvpn3/ovpn3.sh
TimeoutStartSec=0

[Install]
WantedBy=default.target
```
Create ovpn3.timer for delayed startup 
```bash
touch /etc/systemd/system/ovpn3.timer
vim /etc/systemd/system/ovpn3.timer
```
Edit OnBootSec time to match your needs. 4 min should be plenty of time.
```bash
[Unit]
Description=Delay to for OpenVPN3 connection

[Timer]
OnBootSec=4min
Unit=ovpn3.service

[Install]
WantedBy=default.target
```

```bash
systemctl enable ovpn3.service
systemctl enable ovpn3.timer
systemctl status ovpn3.service
systemctl status ovpn3.timer
```
If an issue happens with either the service or the timer double check file permissions, syntax used, device used etc. Systemctl is pretyy good at giving you the exact reason it failed. Now verify time works, I set my timer to 4 minuets which may be excessive but it works.
```bash
systemctl start ovpn3.timer
systemctl list-timers --all
```
If your time had no know errors and you listed the correct net device and VPN ovpn file it should start in 4 min or whatever you set it to and a VPN tunnel will appear when you check:
```bash
ip addr
ip route
```
Show VLAN tunnel. It should show a similarly named device such as tun0:
```bash
ip addr show tun0
```
## Setup Openssh on Host for extra security between machines on your VPN connection. 
Enable for use after reboot
```bash
sudo pacman -Sy openssh
sudo systemctl enable sshd
```
Start openssh sshd service
```bash
sudo systemctl start sshd
```
Verify sshd is running
```bash
sudo systemctl status sshd
```
View manpage the generate public key
```bash
man ssh-keygen
ssh-keygen
```
Connect on client side
```bash
ssh username@hostip
```
## Use ssh copy id for permanent connection
```bash
ssh-copy-id username@hostip

```
# Setup VNC if you need graphical view of devices. Not needed if you only need CLI or have other methods of viewing clients such as virtual machines using libvirt and qemu+ssh. 
## Wayland based wayvnc
Wayvnc is very fast and takes little to no configuration. You must ensure all dependancies are compiled first using AUR or by source code:
* [https://aur.archlinux.org/neatvnc-git.git](https://aur.archlinux.org/neatvnc-git.git)
* [https://aur.archlinux.org/aml-git.git](https://aur.archlinux.org/aml-git.git)
* [https://github.com/any1/wayvnc](https://github.com/any1/wayvnc)
* [https://aur.archlinux.org/packages/wayvnc/] (https://aur.archlinux.org/packages/wayvnc/)
The remaining dependancies can be installed using pacman.
```bash
ssh username@hostvncserver
pacman -S base-devel libglvnd libxkbcommon pixman gnutls
```
Set wayvnc to headless in host computer .bashrc
```bash
export WLR_BACKENDS=headless
export WLR_LIBINPUT_NO_DEVICES=1
```
Launch sway on remote host via ssh if not already running and start vnc host.
Run wayvnc from AUR
```bash
sway &
wayvnc 0.0.0.0
```

## Remote Linux via TigerVNC
```bash
pacman -Sy tigervnc
```
# tigervnc on host
```bash
vncserver
```
Host will announce DISPLAY info upon launch: hostname:1 for display 1. If you run vncserver a second time it will grab hostname:2 followed by hostname:3 and so on. The client should be run with display number after the IP or hostname of the vncserver.
### tigervnc on client
```bash
vncviewer hostip:1
```
Or if connection should be the second instance of the vncserver:
```bash
vncviewer hostip:2
```
# Kill off bad displays on vncserver from host via shh and kill
```bash
ssh username@hostname
vncserver -kill :1
```
# OpenVPN3 with some ovpn files using username and password
Append auth-user-pass followed by PATH to a text file which includes a username and password
```bash
auth-user-pass /etc/openvpn3/auth.txt 
```
Authentication file should look like the following
```bash
username
password
```
Then login normally 
