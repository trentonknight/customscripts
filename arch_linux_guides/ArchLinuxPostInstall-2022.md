# Post install

## Add locale to .bashrc for some ascii based programs
```bash
export LANG=en_US.UTF-8
```
## Add standard user
``` bash
useradd -m -g users -G wheel -s /bin/bash trentonknight
passwd trentonknight
vim /etc/sudoers
trentonknight ALL=(ALL) ALL
```
## Wireless setup with wpa_supplicant
``` bash 
ip link set wlp2s0 up
wpa_supplicant -B -i wlp2s0 -c <(wpa_passphrase SSIDhere "passphrase_here")
dhcpcd wlp2s0
ip link show wlp2s0
```
## Wireless setup with NetworkManager
```bash
systemctl enable NetworkManager
systemctl start NetworkManager
systemctl status NetworkManager
nmcli dev wifi
nmcli dev wifi connect SSID --ask
```
### nmcli SSID is --ask fails
```bash
nmcli dev wifi connect SSID password YourPassword
```

## Add default software

``` bash
pacman -Sy mpd wofi waybar sudo xorg-xwayland wayland linux-firmware neofetch alacritty networkmanager hunspell-en_US firefox sway swayidle swaylock pulseaudio pulseaudio-alsa pulseaudio-bluetooth pulseaudio-equalizer pavucontrol ntp git openssh openssl feh
```

### Add fonts if wanted
```bash
sudo pacman -Sy ttf-dejavu terminus-font ttf-linux-libertine noto-fonts noto-fonts-extra ttf-fira-sans ttf-fira-mono ttf-ubuntu-font-family ttf-croscore ttf-roboto ttf-inconsolata ttf-opensans noto-fonts-emoji texlive-core texlive-fontsextra otf-latin-modern otf-latinmodern-math otf-cascadia-code ttf-cascadia-code woff2-cascadia-code ttf-sarasa-gothic ttf-fira-code ttf-nerd-fonts-symbols
```
## Sync time
```bash
sudo systemctl enable ntpd
timedatectl set-ntp true
timedatectl set-timezone America/New_York
timedatectl status
```

## Add neofetch to terminal for fun
```bash
vim .bashrc
neofetch --ascii_distro BlackArch
```
# SWAY Windows Manager (WAYLAND)
Add gaps to swaywm using i3-gaps. Remove ``default_border`` and hide the ``titlebar_border`` iaw the ``man 5 sway`` page.

``` bash
mkdir -p ~/.config/sway
cp /etc/sway/config ~/.config/sway/config
vim .config/sway/config 
gaps outer 10
gaps inner 20
default_border pixel 0px
default_floating_border none
titlebar_border_thickness 0
output * bg /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_1920x1080.png fill
set $menu wofi --show=drun -I --lines=5 --prompt=""
```

### From wofi manpage regarding configuration
Look at `man 7 wofi` as well.
```bash
Wofi has 3 main files used for configuration. All files are completely optional.

       1.  The config file which defaults to $XDG_CONFIG_HOME/wofi/config.

       2.  The CSS file which defaults to $XDG_CONFIG_HOME/wofi/style.css.

       3.  The colors file which defaults to the pywal cache $XDG_CACHE_HOME/wal/colors.
```

### Add second monitor positioning in swaywm
```bash
swaymsg -t get_outputs
output eDP-1 pos 0 0
output HDMI-A-1 pos 1366 0
```
### Uncomment swayidle already shown in .config
``` bash
### Idle configuration
#
# Example configuration:
#
 exec swayidle -w \
          timeout 1800 'swaylock -f -c 000000' \
          timeout 2000 'swaymsg "output * dpms off"' \
               resume 'swaymsg "output * dpms on"' \
          before-sleep 'swaylock -f -c 000000'
```
### waybar config

* [https://github.com/Alexays/Waybar/wiki/Examples](https://github.com/Alexays/Waybar/wiki/Examples)

* [https://github.com/Egosummiki/dotfiles/tree/master/waybar](https://github.com/Egosummiki/dotfiles/tree/master/waybar)

```bash
pacman -Sy waybar
```
Use Egosummiki theme from above URL or other. Reduce bar in ``.config/sway/config`` to the below and add theme files to ``.config/waybar/`` 

```bash
bar {
swaybar_command waybar
}
```
# Example waybar & wofi theme combo:

Enter waybar config directory.

```bash
cd ~/.config/waybar/
```
Pull config file.

```bash
curl -O https://git.sr.ht/~begs/dotfiles/blob/1c92a56187a56c8531f04dea17c5f96acd9e49c4/.config/waybar/config
```
Pull .css file.

```bash
curl -O https://git.sr.ht/~begs/dotfiles/blob/1c92a56187a56c8531f04dea17c5f96acd9e49c4/.config/waybar/style.css
```
Enter wofi directory.

```bash
cd ~/.config/wofi/
```
Pull wofi .css theme.
```bash
curl -O https://raw.githubusercontent.com/DN-debug/wofi-themes/master/sultai/style.css
```

#### Replace dmenu with fzf fuzzing finder trick

Use fzf to discover all needed application then launch via cli instead.

```bash
sudo pacman -Sy fzf
```

```bash
alias fzfind="pacman -Qq | fzf --preview 'pacm
an -Qil {}' --layout=reverse --bind 'enter:exe
cute(pacman -Qil {} | less)'"
```
### Change bar and client focused/unfocused colors

``` bash
bar {
position top
status_command while date +'%R %d-%b-%y'; do sleep 1; done
colors {
background #323232
statusline #d65219
focused_workspace #d65219 #d65219 #000000
inactive_workspace #d65219 #323232 #d65219
}
}
client.focused #BC3908 #BC3908 #333333 #2e9ef4 #285577
client.unfocused #333333 #222222 #ff6600 #292d2e #222222
```
# i3 Windows Manager (XWindows)
Add gaps 
```bash
gaps outer 10
gaps inner 20
```
Change terminal to alacritty
```bash
# start a terminal
bindsym $mod+Return exec alacritty
```
Use FEH to handle wallpaper background
```bash
exec feh --no-fehbg --bg-scale '/home/trentonknight/Pictures/batman.jpg'
```
## Use betterlockscreen 
### Install dependancies
```bash
git clone https://aur.archlinux.org/i3lock-color.git
cd i3lock-color
makepkg -si
git clone https://aur.archlinux.org/betterlockscreen-git.git
cd betterlockscreen-git
makepkg -si
sudo pacman -Sy xss-lock
```
### Cache an image in betterlockscreen, backend is feh
```bash
betterlockscreen -u ~/Pictures/Forests.png
```
#### i3 config for screen locking. 
Use xss-lock to use systemd sleep with betterlockscreen. Setup key binding to manually lock the screen with mod+shift+x or something else custom.
```bash
exec --no-startup-id xss-lock --transfer-sleep-lock -- betterlockscreen -l 
bindsym $mod+shift+x exec betterlockscreen -l 
```

### Small edits to bar 
```bash
bar {
position top
        status_command i3status
status_command while date +'%Y-%m-%d %l:%M:%S %p'; do sleep 1; done

    colors {
        statusline #ffffff
        background #323232
        inactive_workspace #32323200 #32323200 #5c5c5c
    }
}
```
# Git setup
```bash
git config --global user.email "youmail@protonmail.com"
git config --global user.name "youname"
git config --global core.editor "vim"
```
# Add Gnome Shell extensions
* [https://wiki.gnome.org/Projects/GnomeShellIntegrationForChrome/Installation](https://wiki.gnome.org/Projects/GnomeShellIntegrationForChrome/Installation)
```bash
sudo pacman -S chrome-gnome-shell
gnome-shell --version
```
## Add Material Shell for Sway like tiling windows in Gnome
* [https://extensions.gnome.org/extension/3357/material-shell/](https://extensions.gnome.org/extension/3357/material-shell/)
# edit termite config
```bash
mkdir ~/.config/termite
vim ~/.config/termite/config

[colors]
background = rgba(63, 63, 63, 0.8)
```
# OpenSC config for cac
``` bash
pacman -S ccid opensc
sudo systemctl enable pcscd
sudo systemctl start pcscd
```
#OPENSC

Add the following to opensc.conf to force the selection cac bas
``` bash
vim /etc/opensc.conf
card_drivers = cac, internal
Force_card_driver = cac;
```
# NSSDB
* [https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/Reference/NSS_tools_:_certutil](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/Reference/NSS_tools_:_certutil)
* [https://wiki.archlinux.org/title/Network_Security_Services](https://wiki.archlinux.org/title/Network_Security_Services)

Add opensc library to nssdb

```bash
modutil -dbdir sql:.pki/nssdb/ -add "CAC Module" -libfile /usr/lib/opensc-pkcs11.so
```
Verify install

```bash
certutil -d sql:$HOME/.pki/nssdb -L -h all
```

# DOD Certs

* [https://cyber.mil/pki-pke/end-users/getting-started/linux-firefox/](https://cyber.mil/pki-pke/end-users/getting-started/linux-firefox/)

```bash
[trentonknight@archBOX Downloads]$ unzip unclass-certificates_pkcs7_v5-6_dod.zip
Archive:  unclass-certificates_pkcs7_v5-6_dod.zip
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD.der.p7b
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD.pem.p7b
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD.sha256
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD_DoD_Root_CA_2.der.p7b
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD_DoD_Root_CA_3.der.p7b
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD_DoD_Root_CA_4.der.p7b
  inflating: Certificates_PKCS7_v5.6_DoD/Certificates_PKCS7_v5.6_DoD_DoD_Root_CA_5.der.p7b
  inflating: Certificates_PKCS7_v5.6_DoD/DoD_PKE_CA_chain.pem
  inflating: Certificates_PKCS7_v5.6_DoD/README.txt
```
# firefox with teams
* [https://support.mozilla.org/en-US/kb/enhanced-tracking-protection-firefox-desktop](https://support.mozilla.org/en-US/kb/enhanced-tracking-protection-firefox-desktop)
* [https://docs.microsoft.com/en-us/microsoftteams/troubleshoot/teams-sign-in/sign-in-loop](https://docs.microsoft.com/en-us/microsoftteams/troubleshoot/teams-sign-in/sign-in-loop)

Add the following exceptions under `Cookies and Site Data`:

* https://*.microsoft.com
* https://*.microsoftonline.com
* https://*.teams.skype.com
* https://*.teams.microsoft.com
* https://*.sfbassets.com
* https://*.skypeforbusiness.com

## Alacritty config
``` bash
cp /usr/share/doc/alacritty/example/alacritty.yml .alacritty.yml
```
Copy and paste a new theme into the .alacritty.yml at the bottom


* [https://github.com/rajasegar/alacritty-themes](https://github.com/rajasegar/alacritty-themes)
* [https://github.com/eendroroy/alacritty-theme](https://github.com/eendroroy/alacritty-theme)

Clone alacritty themes and build

```bash
git clone https://aur.archlinux.org/alacritty-themes.git
cd alacritty-themes
makepkg -si
```
Use the following commands to create the correct directory and select a theme: 'Dkeg - unsiftedwheat.yml' is one that matches the previous waybar and wofi color schemes.

```bash
alacritty-themes --create
alacritty-themes
```

### Alacritty Desktop Opacity and LANG

```bash
env:
  # TERM variable
  #
  # This value is used to set the `$TERM` environment variable for
  # each instance of Alacritty. If it is not present, alacritty will
  # check the local terminfo database and use `alacritty` if it is
  # available, otherwise `xterm-256color` is used.
  TERM: alacritty
  LANG: "en_US.UTF-8"
  LC_CTYPE: en_US.UTF-8
  # window should be parallel with env: shown above
window: 
  opacity: 0.8
  # Window dimensions (changes require restart)
```

### If terminal fails boot grub and append the number 2 to the vmlinuz-linux line
``` bash
linux /boot/vmlinuz-linux root=UUID rw loglevel=3 quite 2
```
## Viewing error logs
``` bash
journalctl -p err -b
```
This will show you all messages marked as error, critical, alert, or emergency. The journal implements the standard syslog message levels. You can use either the priority name or its corresponding numeric value. In order of highest to lowest priority, these are:

    0: emerg
    1: alert
    2: crit
    3: err
    4: warning
    5: notice
    6: info
    7: debug

The above numbers or names can be used interchangeably with the -p option. Selecting a priority will display messages marked at the specified level and those above it.

## Add Security
Configure auditD
``` bash
sudo auditctl -w /etc/passwd -p rwxa
sudo auditctl -w /etc/security/
sudo chmod 640 /var/log/audit/audit.log
sudo auditctl -w /etc/passwd -p wa -k passwd_changes
sudo aureport
```
# Terminal and Vim eyecandy with NerdFonts, Starship and vim-devicons
## NerdFonts install
```bash
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
cd nerd-fonts
./install.sh
ls ~/.local/share/fonts/NerdFonts/
```
## Starship install
```bash
pacman -Sy starship
```
### .bashrc
```bash
eval "$(starship init bash)"
```
## vim-devicons and gruvbox theme
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

```bash
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'
Plugin 'pprovost/vim-ps1'
Plugin 'morhetz/gruvbox'
Plugin 'chase/vim-ansible-yaml'
Plugin 'ryanoasis/vim-devicons'

call vundle#end()
filetype plugin indent on
autocmd vimenter * colorscheme gruvbox
set background=dark 
syntax on
set encoding=UTF-8
set guifont=DroidSansMono\ Nerd\ Font\ 11
```
# Radicle
* [how-to-change-default-tmp-to-home-user-tmp](https://serverfault.com/questions/72955/how-to-change-default-tmp-to-home-user-tmp)

Pull radicle using cargo. To avoid overfilling your `/tmp` directory with this large build use cargo `--target-dir`.

```bash
cargo install --force --locked --target-dir="~/Development/seed/" --git https://seed.alt-clients.radicle.xyz/radicle-cli.git radicle-cli
```
Start sshd

```bash
sudo systemctl start sshd
```
Add output from ssh-agent to `.bashrc` if `SSH_AUTH_SOCK` is not seen.

```bash
ssh-agent -s
```
For example, within your `.bashrc`:

```bash
export SSH_AUTH_SOCK=/home/trentonknight/tmp/cnj8/ssh-XXXXXXUtEPBJ/agent.5546
```
Now run rad authentication:

```bash
rad auth 
```
