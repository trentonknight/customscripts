# ARCH LINUX INSTALL 2018
> Set bios to uefi boot only
## Temporary network connection for build
``` bash
iwctl
device list
station wlan0 scan
station wlan0 get-networks
station wlan0 connect SSID
ip link
ping www.linux.org
```


## Verify UEFI drivers loaded
``` bash
ls /sys/firmware/efi/efivars
```

## Clean up old UEFI if needed
Check first.

```bash
efibootmgr -b 0 -B
```

## Setup local time 
``` bash
timedatectl set-ntp true
timedatectl set-timezone America/New_York
timedatectl status
```

## Clean up hard disk for build
``` bash
gdisk /dev/mmcblk0
gdisk /dev/sdb #Or sda, sdc verify what microSD mounts as.
```
* x, z, Y
## Create partitions on MMCBLK0

``` bash
gdisk /dev/mmcblk0
```
* /dev/mmcblk0p1 ( used for future UEFI )
  * +500M EFI System
  * t EF00
* /dev/mmcblk0p2 ( used for BOOT partition)
  * Remaining
  * t 8300

## Create partitions on Lenova Internal drive
* gdisk /dev/sdb
  * Use entire sdb ( Used for /mnt aka the operating system, folders, and packages)
  * t 8300

## Encrypting with luks

Encrypt the partition which contains your sensitive data. The OS which includes your file structure.

```bash
modprobe dm-crypt
modprobe dm-mod
cryptsetup luksFormat -v -s 512 -h sha512 /dev/sdb1
cryptsetup open /dev/sdb1 luks_root
```

## Format Partitions
Now format the UEFI, BOOT and Luks root.

``` bash 
mkfs.vfat -n "EFI" -F32 /dev/mmcblk0p1 
mkfs.ext4 -L BOOT /dev/mmcblk0p2 
mkfs.ext4 -L ROOT /dev/mapper/luks_root
```

## Mount from live image and begin building your OS

You will need to mount the `luks_root` at `/mnt`, make the boot directory and mount the boot partition `mmcblk0p2` and finally create and mount the `efi` folder with `mmcblk0p1` the `EFI` partition. [Luks](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_a_partition) will handle the mounting of `/dev/sdb` on boot after decryption via your GRUB configuration. This will be handled a bit later. For now lets mount these drives:

```bash
mount /dev/mapper/luks_root /mnt
mkdir /mnt/boot
mount /dev/mmcblk0p2 /mnt/boot
mkdir /mnt/boot/efi
mount /dev/mmcblk0p1 /mnt/boot/efi
```

## Find and use best mirrors for packages before installing base system
```bash
pacman -Sy reflector
sudo reflector --country 'United States' --latest 50 --protocol https --download-timeout 60 --save /etc/pacman.d/mirrorlist
```

``` bash 
pacstrap /mnt base linux linux-firmware linux-headers dhcpcd vim networkmanager grub-efi-x86_64 efibootmgr sudo
```
``` bash
genfstab -U /mnt >> /mnt/etc/fstab
```
Append microSD manually to /etc/fstab now or later after build. Use ext4 partition for microSD card.
``` bash
# Static information about the filesystems.
# See fstab(5) for details.

# <file system> <dir> <type> <options> <dump> <pass>
# /dev/sdb
UUID=3bf49773-f38c-49f4-a6e6-74f7d04a2a24	/         	ext4      	rw,relatime	0 1

# /dev/mmcblk0p1
UUID=AFC0-7C07      	/boot         	vfat      	rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro	0 2
```

## Verify all partitions are similar to the example shown above
``` bash
cat /mnt/etc/fstab
blkid
```
## Enter /mnt for edits which will affect build after first reboot
``` bash
arch-chroot /mnt /bin/bash
```
## Setup timezone
``` bash
ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
hwclock --systohc --utc
echo archboX-lenovo > /etc/hostname
vim /etc/hosts
```
``` bash
127.0.0.1    localhost
::1        localhost
127.0.1.1    archboX-lenovo.localdomain    archboX-lenovo
```
## Uncomment en_US.UTF-8 UTF-8 and other needed localizations in 
``` bash
/etc/locale.gen
```
## Generate locale with: 
``` bash
locale-gen
```
## Setup password for root
``` bash
passwd
```
# GRUB configuration

```
vim /etc/default/grub
```
## Sda1 mount for first reboot

Currently if you were to run `dmesg` or `fdisk -l` or whatever you might note `sda1` is currently mounted to your live arch image on a usb. This will change on your first reboot and the internal hard drive will become `/dev/sda` and partition mounted by `grub` with be `/dev/sda1`. This is important to note when running `genfstab -U /mnt >> /mnt/etc/fstab` we instead use `luks` to mount `/dev/sda1`. In a way Luks saves us this confusion but when editing the below `grub` config it is critical you realize what partition will be mounted upon boot.  

```bash
GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda1:luks_root"
```

# mkinitcpio.conf for encrypt

```bash
HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck)
```
Now re-run mkinitcpio

```bash
mkinitcpio -p linux
```





> Exit chroot
``` bash
exit
```
Grub only recongnizes what you have mounted not the physical drive. Fstab for both /mnt and /boot are at the root / of the two hard drives used while grub-install uses the mounted directories.
``` bash
# <device>    <dir>   <type> <options> <dump> <fsck>
/dev/mmcblk0p1 /boot   vfat   defaults    0     2
/dev/sdb      /       ext4   defaults    0     1
```
## Grub install outside of chroot
``` bash
grub-install --target=x86_64-efi --root-directory=/mnt --efi-directory=/boot --bootloader-id=GRUB
```
## Grub mkconfig only after performing a arch-chroot
``` bash
arch-chroot /mnt /bin/bash
```
``` bash
grub-mkconfig -o /boot/grub/grub.cfg
```
## Exit chroot again
``` bash
exit
```
## Verify drives, umount and reboot
``` bash
lsblk -l #review mounted drives
umount -R /dev/mmcblk0p1 /boot
umount -R /dev/sdb /mnt
reboot
```
## Basic Post Install setup
### Add standard user
``` bash
useradd -m -g users -G wheel -s /bin/bash trentonknight
passwd trentonknight
```
### Add standard user to sudoers
```bash
vim /etc/sudoers
trentonknight ALL=(ALL) ALL
```
# Connect to network
```bash
sudo nmcli dev wifi
sudo nmcli dev wifi connect SSIDnamehere password MyPasswordHere
```

