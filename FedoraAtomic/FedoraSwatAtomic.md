# Fedora Atomic Sway
* [fedora atomic sway](https://fedoraproject.org/atomic-desktops/sway)
Fedora Atmoc ships with vi. Other cli editors should be configured in standard `.config` folders in the `$HOME` directory and run inside of `toolbox`. For GUI apps use Flatpak.

## Sway config
Add to `~/.config/sway/config`

```bash
gaps outer 10
gaps inner 20
default_border none
default_floating_border none
titlebar_padding 1
titlebar_border_thickness 0
```
Wget cactus wallpaper
```bash
wget https://gruvbox-wallpapers.pages.dev/wallpapers/irl/cactus.png
```
Change output image
```bash
output * bg ~/Pictures/cactus.png fill
```
Update swaylide `timeouts to

```bash
 exec swayidle -w \
          timeout 1800 'swaylock -f -c 000000' \
          timeout 2000 'swaymsg "output * power off"' resume 'swaymsg "output * power on"' \
          before-sleep 'swaylock -f -c 000000'
```
## Swaylock screen
edit image to match sway config with `vi`

```bash
vi /etc/swaylock/config
```
LIke so
```bash
image=~/Pictures/cactus.png
```
## Nerdfont

```bash
sudo dnf copr enable astrawan/nerd-fonts
sudo dnf -y install nerd-firacode-fonts
```

### Manually adding NerdFonts to `~/.local/share/fonts`

```bash
#!/bin/bash

declare -a fonts=(
	
	FiraCode
	FiraMono
	Go-Mono
	Hack
	JetBrainsMono
	NerdFontsSymbolsOnly
	Noto
	RobotoMono
	ShareTechMono
	Terminus
	Tinos
	Ubuntu
	UbuntuMono
	VictorMono
)

version='2.2.2'
fonts_dir="${HOME}/.local/share/fonts"

if [[ ! -d "$fonts_dir" ]]; then
    mkdir -p "$fonts_dir"
fi

for font in "${fonts[@]}"; do
    zip_file="${font}.zip"
    download_url="https://github.com/ryanoasis/nerd-fonts/releases/download/v${version}/${zip_file}"
    echo "Downloading $download_url"
    wget "$download_url"
    unzip -o "$zip_file" -d "$fonts_dir" -x "*.txt/*" -x "*.md/*"
    rm "$zip_file"
done

find "$fonts_dir" -name '*Windows Compatible*' -delete

fc-cache -fv

```
Now verify

```bash
fc-list | grep "Fira"
```

## waybar gruvbox

```bash
mkdir ~/.config/waybar
cd ~/.config/waybar
```
Pull Waybar config
```bash
curl -O https://git.sr.ht/~begs/dotfiles/blob/1c92a56187a56c8531f04dea17c5f96acd9e49c4/.config/waybar/config
```
Pull css
```bash
curl -O https://git.sr.ht/~begs/dotfiles/blob/1c92a56187a56c8531f04dea17c5f96acd9e49c4/.config/waybar/style.css
```
## Starship for terminal
* [rust](https://www.rust-lang.org/tools/install)
* [starship](https://starship.rs/guide/#%F0%9F%9A%80-installation)
Install rust
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
Install starship

* [starship](https://starship.rs/guide/#%F0%9F%9A%80-installation)

```bash
curl -sS https://starship.rs/install.sh | sh
```

### starship preset
Use gruvbox preset and toml file
```bash
wget https://starship.rs/presets/toml/gruvbox-rainbow.toml
cp gruvbox-rainbow.toml ~/.config/starship.toml
```
Now enbale preset gruvbox-rainbow
```bash
starship preset gruvbox-rainbow -o ~/.config/starship.toml
```


## git configure

```bash
git config --global user.email "youmail@protonmail.com"
git config --global user.name "youname"
git config --global core.editor "nvim"
```

## Google Chrome Install
* [Google Linux Software Repositories](https://www.google.com/linuxrepositories)

## Toolbox run

* [toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox)



