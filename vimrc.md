# Basics of Vundle with one additional plugin for gruvbox theme. Everything else is required
## Download Vundle
```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```
## Edit .vimrc in user home folder
```bash
set nocompatible
    filetype off

    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()

    Plugin 'gmarik/Vundle.vim'
    Plugin 'pprovost/vim-ps1'
    Plugin 'morhetz/gruvbox'
    Plugin 'chase/vim-ansible-yaml'

    "All of your Plugins must be added before the following line"
     call vundle#end()
     filetype plugin indent on
     autocmd vimenter * colorscheme gruvbox
     set background=dark    " Setting dark mode
     syntax on
```
## Over ssh on editable VMs

Add the following to .bashrc is allowed
```bash
export TERM=xterm
source ~/.bashrc
```
