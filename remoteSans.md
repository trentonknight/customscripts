# Windows Host
* [https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)
* [https://docs.microsoft.com/en-us/powershell/scripting/learn/remoting/ssh-remoting-in-powershell-core?view=powershell-7](https://docs.microsoft.com/en-us/powershell/scripting/learn/remoting/ssh-remoting-in-powershell-core?view=powershell-7)
* [https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-7)
## Install OpenSSH
####
Slingshot
```bash
sudo apt-get install openssh-server
sudo systemctl enable ssh
sudo systemctl start ssh
sudo systemctl status ssh
```
### Windows
Verify OpenSSH versions
```powershell
Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'
```
Make sure the below version matches the version printed from the previous command
```powershell
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
Start-Service sshd
# OPTIONAL but recommended:
Set-Service -Name sshd -StartupType 'Automatic'
# Confirm the Firewall rule is configured. It should be created automatically by setup.
Get-NetFirewallRule -Name *ssh*
# There should be a firewall rule named "OpenSSH-Server-In-TCP", which should be enabled
# If the firewall does not exist, create one
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22
```
## Install Powershell 7 on Windows Host
```powershell
msiexec.exe /package PowerShell-7.0.3-win-x64.msi /quiet ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 ENABLE_PSREMOTING=1 REGISTER_MANIFEST=1
```
### Using Powershell 7
Make sure to launch Powershell 7 using pwsh NOT powershell command. Here is an example of a ssh connection double hop, first to a host physical machine the a second hop to a Windows Virtual Machine.
```bash
ssh -J username@hosthop1:22 username@hosthop2
```
```powershell
C:\Users\Sec504>pwsh
PowerShell 7.0.3
Copyright (c) Microsoft Corporation. All rights reserved.

https://aka.ms/powershell
Type 'help' to get help.
```
## Setup Powershell Remote 
```shell
cd $env:ProgramData\ssh\
notepad sshd_config
```
Append the following to the sshd_config
```vim
PasswordAuthentication yes
Subsystem powershell c:/progra~1/powershell/7/pwsh.exe -sshs -NoLogo
```
Then restart sshd service
```shell
Restart-Service sshd
```
## Microsoft powershell container
Install container
```bash
sudo podman run -it mcr.microsoft.com/powershell
```
Inside the Powershell container
```powershell
$session = New-PSSession -Hostname 192.168.122.186 -UserName sec504
Enter-PSSession $session
```
# Slingshot Linux
Install a cli web browser
```bash
wget https://github.com/browsh-org/browsh/releases/download/v1.6.4/browsh_1.6.4_linux_amd64.deb
sudo dpkg -i browsh_1.6.4_linux_amd64.deb
```
# Podman Images
* [http://docs.podman.io/en/latest/Introduction.html](http://docs.podman.io/en/latest/Introduction.html)
Podman find Image for use
```bash
podman search powershell
```
List all local images
```bash
podman images
```
Run image using Image ID shown from above
```bash
sudo podman run -it 
#File sharing over ssh with double hop
```bash
sudo pacman -Syu fuse2 sshfs
sudo insmod fuse2
```
```bash
ssh -J username@hosthop1:22 username@hosthop2
sshfs -p 2222 username@hosthop2:/home/host2user/folder2share /home/myuser/foofolder
```

