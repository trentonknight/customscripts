# Requirements that should be covered by previous classes taken at Regis or ECPI University
* MA 111 Precalculus Algebra and Trigonometry (Regis classes MT201, MT320, MT415)
* MA 141 Calculus I (Regis class 360A Calculus I)
* MA 241 Calculus II (Regis class 360B Calculus II)
* ECE 109 Introduction to Computer Systems (ECPI CS100 + BS in CS from Regis University) 

# Undergrad Non-Degree Studies (NDS) student for needed requirements.
## Core Classes (Units 19)
[Non-Degree Studies (NDS) student](https://registrar.ncsu.edu/nds/)

1. PY 205: Physics for Engineers and Scientists I (Units 3, Corequisite PY 206, Online)
  * PY 206: Physics for Engineers and Scientists I Laboratory (Units 1, Online)
3. ECE 200 Introduction to Signals, Circuits and Systems (Units 4, MA 241 & PY 205, Online)
4. ECE 211 Electric Circuits (Units 4, Corequisite ECE 220, Online)
  * ECE 220 Analytical Foundations of Electrical and Computer Engineering (Units 3, ECE 200, Online)
5. ECE 302 Microelectronics (Units 4, Req 211, Online)
### Needed for Comp Architecture (Units 3)
* ECE 212 Fundamentals of Logic Design (Units 3, Req ECE 109, Online)

# [Masters of Science in Computer Engineering](https://www.engineeringonline.ncsu.edu/programs-and-courses/graduate/master-of-science-in-computer-engineering/)(31 credit hours req)
* ECE 600: ECE Graduate Orientation (CPE, Units 1, Required, Online)
## Graduate level ECE courses (21 Units req)
### Computer Architecture and Systems Plan of Work (Units 12)
[computer-architecture-and-systems-plan-of-work](https://www.ece.ncsu.edu/grad/masters/computer-architecture-and-systems-plan-of-work/)
#### Suggested courses for VLSI Systems
* ECE 546: VLSI Systems Design (CPE, Units 3, ECE 302,  Online)
* ECE 564: ASIC and FPGA Design with Verilog (CPE, Units 3, ECE 212, Online)
##### DEPTH 700 courses
* ECE 745: ASIC Verification (CPE, Units 3, ECE 564, Online)
* ECE 748: Advanced Functional Verification with Universal Verification Methodology (CPE, Units 3, ECE 745)
#### BREADTH: Three ECE Courses in [different specialty](https://ece.ncsu.edu/wp-content/uploads/2018/03/ECECourseDetail.pdf)
##### Suggested courses for Software
* ECE 517: Object Oriented Design and Development (CPE, Units 3)
##### Suggested courses for Embedded Systems
* ECE 560: Embedded Systems Architecture (CPE, Units 3)
##### Suggested courses for Computer Architecture
* ECE 506: Architecture of Parallel Computers (CPE, Units 3)
#### Additional courses to meet 31 credits
* ECE 561: Embedded System Design (CPE, Units 3, ECE560)
#### Non-ECE
* ECE 505: Design and Analysis of Algorithms (EE, Units 3, CSC226 and CSC316) 
* CSC 630: Master's Independant Study (Units 3)

# References
## Plan of Work
[Plan of work bottom of ECE page here](https://www.ece.ncsu.edu/grad/masters/)
## Quantum Information future classes
[quantum.ncsu](https://quantum.ncsu.edu/training-and-education/)
## Registration & Records Search
[Course Catalog Search](https://www.acs.ncsu.edu/php/coursecat/directory.php#course-search-results)
