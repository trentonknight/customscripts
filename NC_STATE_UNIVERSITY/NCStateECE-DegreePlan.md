# [Masters of Science in Computer Engineering](https://www.engineeringonline.ncsu.edu/programs-and-courses/graduate/master-of-science-in-computer-engineering/)
* ECE 600: ECE Graduate Orientation (Units 1, Required, Online)

### Computer Architecture and Systems Plan of Work (Units 12)
[computer-architecture-and-systems-plan-of-work](https://www.ece.ncsu.edu/grad/masters/computer-architecture-and-systems-plan-of-work/)

* ECE 546: VLSI Systems Design (Units 3, ECE 302,  Online)
* ECE 564: ASIC and FPGA Design with Verilog (Units 3, ECE 212, Online)
* ECE 745: ASIC Verification (Units 3, ECE 564, Online)
* ECE 748: Advanced Functional Verification with Universal Verification Methodology (Units 3, Not scheduled)

### Needed for Comp Architecture (Units 3)
* ECE 212 Fundamentals of Logic Design (Units 3, Req ECE 109, Online)

# Requirements that should be covered by previous classes taken at Regis University
* MA 111 Precalculus Algebra and Trigonometry (Regis classes MT201, MT320, MT415)
* MA 141 Calculus I (Regis class 360A Calculus I)
* MA 241 Calculus II (Regis class 360B Calculus II)
* ECE 109 Introduction to Computer Systems (ECPI CS100 + Regis MT320, CS370) 
* CSC 316: Data Structures and Algorithms (CS 362 & CS 372)
* CSC 226: Discrete Mathematics for Computer Scientists (Regis MT320 Intro to Discrete Mathematics)

# Undergrad Non-Degree Studies (NDS) student for needed requirements.
## Core Classes (Units 19)
[Non-Degree Studies (NDS) student](https://registrar.ncsu.edu/nds/)

1. PY 205: Physics for Engineers and Scientists I (Units 3, MA141, Corequisite PY 206, Online)
  * PY 206: Physics for Engineers and Scientists I Laboratory (Units 1, MA241 or MA241, Online)
3. ECE 200 Introduction to Signals, Circuits and Systems (Units 4, MA241 & PY 205, Online)
4. ECE 211 Electric Circuits (Units 4, Corequisite ECE 220, Online)
  * ECE 220 Analytical Foundations of Electrical and Computer Engineering (Units 3, ECE 200, Online)
5. ECE 302 Microelectronics (Units 4, Req 211, Online)

## Graduate level courses outside ECE (Units 9)

* CSC 505: Design and Analysis Of Algorithms (Units 3, CS316 & CSC226, Online)
* CSC 520: Artificial Intelligence I (Units 3, CSS316 & CSC226, Online)
* CSC 565: Graph Theory (Units 3, CSC 226, Online)


# References
## Plan of Work
[Plan of work bottom of ECE page here](https://www.ece.ncsu.edu/grad/masters/)
## Quantum Information future classes
[quantum.ncsu](https://quantum.ncsu.edu/training-and-education/)
## Registration & Records Search
[Course Catalog Search](https://www.acs.ncsu.edu/php/coursecat/directory.php#course-search-results)
