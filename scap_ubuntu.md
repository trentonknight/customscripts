# Install openscap tools on Ubuntu
* [https://www.open-scap.org/](https://www.open-scap.org/)
```bash
apt install libopenscap8
```
# Download SCAP file for matching device, operating system or software.
* [https://public.cyber.mil/stigs/scap/](https://public.cyber.mil/stigs/scap/)
Example use of oscap tool with SCAP 1.2 Content for Ubuntu 18.04:

```bash
oscap info U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark.xml
```
Details shown are as follows:
```bash
Document type: Source Data Stream
Imported: 2021-06-21T21:37:43

Stream: scap_mil.disa.stig_datastream_U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark
Generated: 2021-06-21T21:37:43
Version: 1.2
Checklists:
	Ref-Id: scap_mil.disa.stig_cref_U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark-xccdf.xml
		Status: accepted
		Generated: 2021-06-16
		Resolved: false
		Profiles:
			Title: I - Mission Critical Classified
				Id: xccdf_mil.disa.stig_profile_MAC-1_Classified
			Title: I - Mission Critical Public
				Id: xccdf_mil.disa.stig_profile_MAC-1_Public
			Title: I - Mission Critical Sensitive
				Id: xccdf_mil.disa.stig_profile_MAC-1_Sensitive
			Title: II - Mission Support Classified
				Id: xccdf_mil.disa.stig_profile_MAC-2_Classified
			Title: II - Mission Support Public
				Id: xccdf_mil.disa.stig_profile_MAC-2_Public
			Title: II - Mission Support Sensitive
				Id: xccdf_mil.disa.stig_profile_MAC-2_Sensitive
			Title: III - Administrative Classified
				Id: xccdf_mil.disa.stig_profile_MAC-3_Classified
			Title: III - Administrative Public
				Id: xccdf_mil.disa.stig_profile_MAC-3_Public
			Title: III - Administrative Sensitive
				Id: xccdf_mil.disa.stig_profile_MAC-3_Sensitive
			Title: CAT I Only
				Id: xccdf_mil.disa.stig_profile_CAT_I_Only
		Referenced check files:
			U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark-oval.xml
				system: http://oval.mitre.org/XMLSchema/oval-definitions-5
Checks:
	Ref-Id: scap_mil.disa.stig_cref_U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark-oval.xml
	Ref-Id: scap_mil.disa.stig_cref_U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark-cpe-oval.xml
Dictionaries:
	Ref-Id: scap_mil.disa.stig_cref_U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark-cpe-dictionary.xml
```
Using these details create your scan utilizing the PROFILE_ID, ARF_FILE, REPORT_FILE and SOURCE_DATA_STREAM_FILE as follows:
* [https://static.open-scap.org/openscap-1.3/oscap_user_manual.html#_getting_scap_content](https://static.open-scap.org/openscap-1.3/oscap_user_manual.html#_getting_scap_content)
```bash
oscap xccdf eval \
--profile PROFILE_ID \
--results-arf ARF_FILE \
--report REPORT_FILE SOURCE_DATA_STREAM_FILE
```
* PROFILE_ID is the ID of an XCCDF profile
* ARF_FILE is the file path where the results in SCAP results data stream format (ARF) will be generated
* REPORT_FILE is the file path where a report in HTML format will be generated
* SOURCE_DATA_STREAM_FILE is the file path of the evaluated SCAP source data stream

For example
```bash
oscap xccdf eval \
--profile xccdf_mil.disa.stig_profile_MAC-1_Public \
--results-arf arf.xml \
--report report.html \
U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark.xml

```
Use firefox to view report
```bash
firefox report.html
```
# Use --stig-viewer option for evaulating an SCAP data stream other than a STIG provided by DISA.
Get info first for profile from STIG
* [https://public.cyber.mil/stigs/](https://public.cyber.mil/stigs/)
```bash
oscap info U_CAN_Ubuntu_20-04_LTS_V1R1_Manual_STIG/U_CAN_Ubuntu_20-04_LTS_STIG_V1R1_Manual-xccdf.xml 
```

```bash
oscap xccdf eval \
--profile MAC-2_Public \
--stig-viewer results-stig.xml U_CAN_Ubuntu_20-04_LTS_V1R1_Manual_STIG/U_CAN_Ubuntu_20-04_LTS_STIG_V1R1_Manual-xccdf.xml 
```
# Run remediation with SCAP data stream
```bash
oscap xccdf eval \
--remediate --profile xccdf_mil.disa.stig_profile_MAC-1_Public \
--results scan-xccdf-remediate-results.xml \
U_CAN_Ubuntu_18-04_V2R3_STIG_SCAP_1-2_Benchmark.xml 
```
Check the results:
```bash
oscap info scan-xccdf-remediate-results.xml
```
