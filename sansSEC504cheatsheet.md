#SEC504 Cheatsheet
## Lab 1.1
Extra CLI command not used in Lab 1.1
### Get-NetTCPConnection
This command gets all current TCP connections. Run netcat and then view listener as a established connection.
```powershell
nc -l -p 2222
```
Now check the established nc listener connection.
```powershell
Get-NetTCPConnection -State Established
```
### Get-NetRoute
[Get-NetRoute](https://docs.microsoft.com/en-us/powershell/module/nettcpip/get-netroute?view=win10-ps)
```powershell
Get-NetRoute -DestinationPrefix "0.0.0.0/0" | Select-Object -ExpandProperty "NextHop"
```
### Find-Netroute
[This command finds a NetIPAddress object and NetRoute object to use to access the IP address 10.79.197.200. The command returns two objects. The first object is the local IP address. The second object is the local route that includes the selected connectivity path and next hop.](https://docs.microsoft.com/en-us/powershell/module/nettcpip/find-netroute?view=win10-ps)
```powershell
Find-NetRoute -RemoteIPAddress "10.79.197.200"
```
### Get-Process
```powershell
Get-Process | Format-List *
```
With Process priority
```powershell
$A = Get-Process
$A | Get-Process | Format-Table -View priority
```
Get-Process or several by name in formatted list
```powershell
Get-Process winword, explorer | Format-List *
```
[This command gets all processes that have a working set greater than 20 MB. It uses the Get-Process cmdlet to get all running processes. The pipeline operator | passes the process objects to the Where-Object cmdlet, which selects only the object with a value greater than 20,000,000 bytes for the WorkingSet property.](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.management/get-process?view=powershell-7)
```powershell
Get-Process | Where-Object {$_.WorkingSet -gt 20000000}
```
## Lab.1.4 Malware Investigation

Some missing items from Lab
# Network Adapter setting up DnsClientServer
```powershell
Get-NetAdapter
Get-NetIpConfiguration
Set-DnsClientServerAddress -InterfaceIndex "ifIndex shown from Get-NetAdaper" -ServerAddress ("VM IP Address Here")
```
If Malware example will not run, remove and reinstall
```powershell
Unregister-ScheduledTask -TaskName "Analytics Backup"
```
Check to verify it running
```
netstat -ano | Select-String 53
```
## Linux Olympian CLI
Good command
```bash
find /var /home -iname "foo*" -exec cat {} \;
```


