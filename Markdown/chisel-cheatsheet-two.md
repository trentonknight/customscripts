# podman install anaconda3 jupyter
```bash
podman search continuumio
```
## Create a similar Dockerfile as below
```text
FROM continuumio/anaconda3
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install gnupg -y
RUN echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
RUN curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install sbt -y
#jupyter lab setup
RUN conda install -y -c conda-forge nodejs jupyterlab jupyterthemes \
jupyterlab-git
RUN jupyter labextension enable jupyterlab_git
RUN jt -t gruvboxd
RUN jupyter labextension install @rahlir/theme-gruvbox
RUN jupyter labextension enable @rahlir/theme-gruvbox
#Expose the default httpd port 8888
EXPOSE 8888
RUN mkdir /opt/notebooks
```
## Use podman for initial image creation
``` bash
podman build -t chiselbook .
podman images
```
## Test build
``` bash
podman run -it -p 8888:8888 localhost/chiselbook jupyter \
lab --allow-root --ip 0.0.0.0 --no-browser
```
## Install notebook themes
```bash
podman run localhost/chiselbook jt -l
podman run localhost/chiselbook jt -t gruvboxd
podman ps -l
podman commit 18ea6574aad5 newname
podman images
```
## Install Jupyter Lab Extensions
``` bash
podman run localhost/chiselbook jupyter \
labextension install @rahlir/theme-gruvbox

podman run localhost/chiselbook jupyter \
labextension enable @rahlir/theme-gruvbox

podman ps -l
podman commit 18ea6574aad5 newname
podman images
```
## Run notebook
``` bash
podman run -it -p 8888:8888 localhost/newname jupyter \
notebook --allow-root --ip 0.0.0.0 --no-browser
```
## Or run Jupyter lab
``` bash
podman run -it -p 8888:8888 localhost/newname jupyter \
lab --allow-root --ip 0.0.0.0 --no-browser
```
## Cleaning up containers
``` bash
buildah ps
buildah rm chiselbook
```
## Cleaning up Images
``` bash
buildah images
buildah rmi --force localhost/chiselbook
```

 