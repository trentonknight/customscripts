# podman install jupyter almond
```bash
podman search debian
```
## Create a similar Dockerfile as below
```text
FROM debian
RUN apt-get update -y
RUN apt-get upgrade -y
RUN apt-get install curl gnupg default-jdk vim -y
RUN echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
RUN curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add
RUN apt-get update -y
RUN apt-get install sbt -y
```
## Use podman for initial image creation
``` bash
podman build -t scala-sbt-con .
podman images
podman run --name sbt-con -it localhost/scala-sbt-con /bin/bash
podman commit ba4137f604fe sbt15nov19
```
