# podman install anaconda3 jupyter
```bash
podman search almond
```
## Create a similar Dockerfile as below
```text
FROM almondsh/almond:latest
USER root
RUN apt-get update && apt-get install -y graphviz git
RUN git clone https://github.com/freechipsproject/chisel-bootcamp.git
USER $NB_UID
RUN conda install -y -c conda-forge nodejs jupyterlab jupyterthemes jupyterlab-git
RUN jupyter labextension install @jupyterlab/plotly-extension
RUN jupyter labextension install @rahlir/theme-gruvbox
RUN jupyter labextension enable jupyterlab_git
RUN jupyter labextension enable @rahlir/theme-gruvbox
RUN jupyter labextension enable @jupyterlab/plotly-extension
COPY --chown=1000:100 notebooks/ $HOME
RUN jt -t gruvboxd
#Expose the default httpd port 8888
EXPOSE 8888

```
## Use podman for initial image creation
``` bash
podman build -t chiselbook .
podman images
```
## Test build
``` bash
podman run -it -p 8888:8888 localhost/chiselbook jupyter \
lab --allow-root --ip 0.0.0.0 --no-browser
```
## Install notebook themes
```bash
podman run localhost/chiselbook jt -l
podman run localhost/chiselbook jt -t gruvboxd
podman ps -l
podman commit 18ea6574aad5 newname
podman images
```
## Install Jupyter Lab Extensions
``` bash
podman run localhost/chiselbook jupyter \
labextension install @rahlir/theme-gruvbox

podman run localhost/chiselbook jupyter \
labextension enable @rahlir/theme-gruvbox

podman ps -l
podman commit 18ea6574aad5 newname
podman images
```
## Run notebook
``` bash
podman run -it -p 8888:8888 localhost/newname jupyter \
notebook --allow-root --ip 0.0.0.0 --no-browser
```
## Or run Jupyter lab
``` bash
podman run -it -p 8888:8888 localhost/newname jupyter \
lab --allow-root --ip 0.0.0.0 --no-browser
```
## Cleaning up containers
``` bash
buildah ps
buildah rm chiselbook
```
## Cleaning up Images
``` bash
buildah images
buildah rmi --force localhost/chiselbook
```

 