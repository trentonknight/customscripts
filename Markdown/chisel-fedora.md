# podman install anaconda3 jupyter
```bash
podman search fedora
```
## Create a similar Dockerfile as below
```text
# Base on the Fedora
FROM fedora:latest
RUN echo "Updating all fedora packages"; \
dnf -y update; dnf -y clean all;
RUN sudo dnf -y install sbt
```
## Use buildah for initial image creation
``` bash
podman build -t chiselbook .
podman images
```
