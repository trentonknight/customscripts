# Copy default config for reference and use after creating needed directory
```bash
mkdir -p .config/alacritty/colors
sudo cp /usr/share/doc/alacritty/example/alacritty.yml ~/.config/alacritty/
```
## Set Opacity of Terminal
```bash
# Background opacity
#
# Window opacity as a floating point number from `0.0` to `1.0`.
# The value `0.0` is completely transparent and `1.0` is opaque.
background_opacity: 0.8
```
## Install alacritty-colorscheme
```bash
pip install --user alacritty-colorscheme
```
Git clone to new color directory
```bash
DEST="$HOME/.config/alacritty/colors"
git clone https://github.com/aaron-williamson/base16-alacritty.git $DEST
```
Adapt Swaywm config to cycle through themes
```bash
set $color_dir $HOME/.config/alacritty/colors
set $light_color base16-gruvbox-light-soft.yml
set $dark_color base16-gruvbox-dark-soft.yml

# Toggle between light and dark colorscheme
bindsym $mod+Shift+n exec alacritty-colorscheme -C $color_dir -t $light_color $dark_color

# Toggle between all available colorscheme
bindsym $mod+Shift+m exec alacritty-colorscheme -C $color_dir -T

# Get notification with current colorscheme
bindsym $mod+Shift+b exec notify-send "Alacritty Colorscheme" `alacritty-colorscheme -C $color_dir -s`
```

