# Risc-V environment
### Using pacman as of March 2020
``` bash
sudo pacman -Syu riscv64-linux-gnu-binutils riscv64-linux-gnu-gcc riscv64-linux-gnu-gdb riscv64-linux-gnu-glibc riscv64-linux-gnu-linux-api-headers 
```
Remove Arch repository version fully
``` bash
sudo pacman -Rcns riscv64-linux-gnu-binutils riscv64-linux-gnu-gcc riscv64-linux-gnu-gdb riscv64-linux-gnu-glibc riscv64-linux-gnu-linux-api-headers 
```
### From source code
#### Build Riscv Linux toolchain
``` bash
sudo pacman -Syu base-devel autoconf automake libtool curl wget gawk bison patchutils bc flex gmp texinfo patchutils gcc linux-api-headers glibc gdb-common guile
```
Get the latest source from the official riscv repository
``` bash
git clone --recursive https://github.com/riscv/riscv-gnu-toolchain
cd riscv-gnu-toolchain
```
Build for Newlib cross-compiler. Configuration should use core architecture. For example Ibex is a small 32 bit RISC-V CPU core (RV32IMC/EMC).
``` bash

make ./configure --prefix=/opt/riscv32 --enable-multilib --disable-linux --disable-gdb --with-arch=rv32imc --with-abi=ilp32
``` 
Edit .bashrc
``` bash
vim ~\.bashrc
export RISCV=/opt/riscv/
export PATH=$PATH:$RISCV/bin
export PATH=$PATH:$RISCV/lib
export PATH=$PATH:$RISCV/include
export RISCV_PATH=/opt/riscv/bin/riscv64-unknown-elf-gcc-9.2.0
export PATH=$PATH:$RISCV_PATH/bin
export PATH=$PATH:$RISCV_PATH/lib
export PATH=$PATH:$RISCV_PATH/include
export RISCV32_PATH=/opt/riscv/riscv32-unknown-linux-gnu
export PATH=$PATH:$RISCV32_PATH/bin
export PATH=$PATH:$RISCV32_PATH/lib
export PATH=$PATH:$RISCV32_PATH/include
```
#### The xPack GNU RISC-V Embedded GCC
##### Install Node
[nodejs downloads](https://nodejs.org/en/download/)
``` bash
wget https://nodejs.org/dist/v12.16.1/node-v12.16.1-linux-x64.tar.xz
tar -xf node-v12.16.1-linux-x64.tar.xz
sudo mv node-v12.16.1-linux-x64 /opt
```
Add nodejs to .bashrc PATH
``` bash
vim ~/.bashrc
export PATH=${PATH}:/opt/node-v12.16.1-linux-x64/bin
```
Update nodejs
``` bash
sudo npm install -g n
sudo n stable
sudo npm install --global xpm@latest
which xpm
```
Install xPack riscv-none-embed-gcc
``` bash
xpm install --global @xpack-dev-tools/riscv-none-embed-gcc@latest
```
### From Sifive Freedom E SDK
* [SiFive Freedom E SDK Github](https://github.com/sifive/freedom-e-sdk)
```bash
git clone https://github.com/sifive/freedom.git
cd freedom
#Run this command to update subrepositories used by freedom
git submodule update --init --recursive
```
# Risc-v cores
* [Risc-c cores list](https://github.com/sifive/riscv-cores-list)
* [LowRISC](https://www.lowrisc.org/)
## Ibex Core
* [Ibex Core](https://ibex-core.readthedocs.io/en/latest/introduction.html)
* [Ibex Core on Arty A7](https://github.com/lowRISC/ibex/tree/master/examples/fpga/artya7)
### Ibex dependancies
#### fusesoc
[fusesoc](https://github.com/olofk/fusesoc)
```bash
sudo pacman -Syu python
git clone https://github.com/olofk/fusesoc.git
cd fusesoc
sudo pip install -e .
```
#### srecord
[srecord](http://srecord.sourceforge.net/download.html)
The SRecord package is a collection of powerful tools for manipulating EPROM load files.
``` bash
pacman -Syu boost
wget http://srecord.sourceforge.net/srecord-1.64.tar.gz
cd srecord-1.64
./configure
sudo make install
```
[srecord documentation](http://srecord.sourceforge.net/srecord-1.64.pdf)

## Prep for Scala, Chisel, ChipYard
### Freechips Project
* [freechipsproject](https://github.com/freechipsproject)
* [Chisel 3](https://github.com/freechipsproject/chisel3)
* [Chisel cheatsheet](https://www.overleaf.com/project/5cf6b7bc17e6773ee4149f7e)
* [sbt](https://www.scala-sbt.org/download.html)

# Sifive 
* [SiFive documentation](https://www.sifive.com/documentation/)
* [SiFive Freedom E310 Arty FPGA Dev Kit Getting StartedGuide](https://sifive.cdn.prismic.io/sifive%2Fed96de35-065f-474c-a432-9f6a364af9c8_sifive-e310-arty-gettingstarted-v1.0.6.pdf)
* [SiFive Github](https://github.com/sifive/)

## OpenOCD
``` bash
pacman -Syu openocd
```
### Qemu Risc-V
[Qemu RISCV platform](https://wiki.qemu.org/Documentation/Platforms/RISCV)

