#!/bin/bash
#https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads

export GNURM=/opt/gcc-arm-none-eabi-8-2019-q3-update/
export PATH=$PATH:$GNURM/bin
export PATH=$PATH:$GNURM/lib

export ARM_NONE_EABI=/opt/gcc-arm-none-eabi-8-2019-q3-update/arm-none-eabi
export PATH=$PATH:$ARM_NONE_EABI/bin
export PATH=$PATH:$ARM_NONE_EABI/lib
export PATH=$PATH:$ARM_NONE_EABI/include

export ARM_NONE_EABI_GDB=/opt/gcc-arm-none-eabi-8-2019-q3-update/arm-none-eabi/share/gdb/
export PATH=$PATH:ARM_NONE_EABI_GDB/python/gdb
export PATH=$PATH:ARM_NONE_EABI_GDB/syscalls
export PATH=$PATH:ARM_NONE_EABI_GDB/system-gdbinit

