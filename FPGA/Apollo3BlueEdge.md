# SparkFun Apollo 3 (EDGE Board)
## Install ch34 connector with USB-C
git clone https://github.com/juliagoda/CH341SER.git
lsmod | grep ch34

sudo rmmod ch341

vim /etc/modprobe.d/blacklist.conf

``` bash
blacklist ch341
```

