# Arty A7
* [Digilent Arty A7 Programable Logic Reference Page](https://reference.digilentinc.com/reference/programmable-logic/arty-a7/start) 
* [Arty-A7-35-Master.xdc](https://github.com/Digilent/digilent-xdc/blob/master/Arty-A7-35-Master.xdc)
* [Arty A7 schematic](https://reference.digilentinc.com/_media/reference/programmable-logic/arty-a7/arty_a7_sch.pdf)
* [Arty A7 reference manual](https://reference.digilentinc.com/reference/programmable-logic/arty-a7/reference-manual)
## Xilinx software on Arch
* [Arch Linux with Arty A7](https://wiki.archlinux.org/index.php/Xilinx_Vivado#Dependencies)
* [Installing vivado](https://reference.digilentinc.com/vivado/installing-vivado/start)
* [Vivado Downloads and Documentation](https://www.xilinx.com/support/download.html)
## Dependancies
``` bash
git clone https://aur.archlinux.org/ncurses5-compat-libs.git
sudo pacman -S --needed base-devel
cd ncurses5-compat-libs/
gpg --recv-keys C52048C0C0748FEE227D47A2702353E0F7E48EDB
makepkg -si
sudo pacman -Sy libpng12 gtk2
```
## Launch latest version of the Xilinx Unified Installer for Linux
``` bash
sudo ./Xilinx_Unified_2019.2_1106_2127_Lin64.bin
```
### Install Xilinx drivers
The below example set the majority of udev rules.
```bash
${vivado_install_dir}/data/xicom/cable_drivers/lin64/install_script/install_drivers/install_drivers
```
# udev rules for Olimex USB
```bash
vim /etc/udev/rules.d/99-openocd.rules
```
Ensure that the Olimex USB has the required permissions.
```bash
# These are for the Olimex Debugger for use with E310 Arty Dev Kit
SUBSYSTEM=="usb", ATTR{idVendor}=="15ba", ATTR{idProduct}=="002a", MODE="664",
GROUP="plugdev"
SUBSYSTEM=="tty", ATTRS{idVendor}=="15ba", ATTRS{idProduct}=="002a", MODE="664",
GROUP="plugdev"
```
# Using screen
``` bash
sudo screen /dev/ttyUSB1 115200
```
A few example screen commands from the screen manpage
``` bash
C-a d,             (detach)          Detach screen  from
C-a C-d                              this terminal.
C-a B              (pow_break)       Reopen the terminal line and send break.
---------------------------------------------------------
C-a c,             (screen)          Create a new window
C-a C-c                              with a shell and switch to that window.
C-a F              (fit)             Resize  the  window to  the current region size.
---------------------------------------------------------
C-a k,             (kill)            Destroy     current
C-a C-k                              window.
```

## If needed set the environment 
``` bash
export VIVADO="/tools/Xilinx/Vivado/2019.2/bin"
export PATH=$PATH:$VIVADO/bin
export PATH=$PATH:$VIVADO/lib
export PATH=$PATH:$VIVADO/include
```
# Vivado Colors for Console
* [gruvbox](https://github.com/morhetz/gruvbox)
## Text Editor
``` bash
Background: #282828
Foreground: #ebdbb2
Current Line Background: #3c3836
Find Highlight Background: #7c6f64
Selection Background: #d65d0e
Matching Words Background: #689d6a
Current Breakpoint Background: #98971a
Cursor Color: #a89984
```
### Text Editor XDC
```bash
Normal: 
Line Comments:
Strings:
Boolean/NULL:
Variables:
Tcl Keywords:
Xdc Keywords:
Invalid:
```
## tc console colors
``` bash
Background: 40, 40, 40 
Foreground: 235, 219, 178
Find Highlight Background: 102,92,84
Selection Color: 214, 93, 14
Matching Words Background: 142, 192, 124
Command Text: 69, 133, 136
Error Text: 251, 73, 52
Warning Text: 250, 189, 47
Cursor Text: 146, 131, 116
``` 
# Verilog
* [Zipcpu](https://zipcpu.com/tutorial/)
* [Verilator HDL simulator](https://www.veripool.org/wiki/verilator)
# LiteX
* [litex-hub](https://github.com/litex-hub)
# FuPy
* [FuPy](https://fupy.github.io/)
* [Micropython](http://micropython.org/)
* [FuPy on Digilent Arty A7](https://github.com/timvideos/litex-buildenv/wiki/HowTo-FuPy-on-a-Digilent-Arty-A7)
* [LiteX FPGA 101 with](https://github.com/litex-hub/fpga_101)

# Chisel
* [freechipsproject](https://github.com/freechipsproject)
* [Chisel language](https://www.chisel-lang.org/)
# SymbiFlow
* [symbiflow documentation](https://symbiflow.readthedocs.io/en/latest/)
* [symbiflow github](https://github.com/SymbiFlow)
``` bash
sudo pacman -Syu cmake
git clone https://github.com/SymbiFlow/symbiflow-arch-defs.git
make env
make all_conda
```
# FPGA HDL Stages
## Synthesis
[Synthesis](https://symbiflow.readthedocs.io/en/latest/toolchain-desc/design-flow.html) is the process of converting input Verilog file into a netlist, which describes the connections between different block available on the desired FPGA chip. However, it is worth to notice that these are only logical connections. So the synthesized model is only a draft of the final design, made with the use of available resources.
### Yosys 
Yosys is a framework for Verilog RTL synthesis. It currently has extensive Verilog-2005 support and provides a basic set of synthesis algorithms for various application domains. 
* [Yosys](http://www.clifford.at/yosys/)
### VTR
* [Verilog-to-Routing’s documentation](https://docs.verilogtorouting.org/en/latest/)


