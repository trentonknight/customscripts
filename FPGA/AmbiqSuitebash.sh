#!/bin/bash
#https://www.ambiqmicro.com/mcu/
# https://github.com/sparkfun/SparkFun_Edge_BSP clone to root of SDK /boards
#git clone https://github.com/sparkfun/SparkFun_Edge_BSP $AMB_ROOT/boards
export AMB_ROOT=/opt/AmbiqSuite/
export PATH=$PATH:$AMB_ROOT/boards
export PATH=$PATH:$AMB_ROOT/CMSIS
export PATH=$PATH:$AMB_ROOT/mcu
export PATH=$PATH:$AMB_ROOT/tools
export PATH=$PATH:$AMB_ROOT/utils


